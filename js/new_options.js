$(function () {
    $('.close').unbind('click').bind('click', function (event) {
        localStorage.removeItem('url');
        event.preventDefault();
        window.close();
    });

    const counter_msg = document.getElementById('count_msg');
    if (counter_msg) {
        counter_msg.addEventListener("wheel", onWheelMsg);
        const start_count = getCount('count_msg_options');
        $('#count_msg').text(start_count);
    }

    getLetters();

    $(document).on('click', '.single_msg', function () {
        const massages_id = $(this).data('id');
        const color = getColor(massages_id);
        $('.picker').data('id', massages_id).val(color).click();
    });

    $(document).on('change', '.picker', function () {
        const massages_id = $(this).data('id');
        const color = $(this).val();
        localStorage[massages_id] = color;
        $('.el_'+massages_id).css('color', color);
    });
});

function getLetters() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", defaultUrl, true);
    xhr.send(null);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.responseText) {
                const data = xhr.responseText;
                const count = getCount('count_msg_options') -1;
                $('.body').html(getMessages(data, count));
            }
        }
    }
}

function getSingleMessage(data, post) {
    const title = $('.nim-dialog ._im_dialog_link', data).eq(post).text();
    const massages_id = +$('.nim-dialog ._im_dialog_link', data).eq(post).closest('.nim-dialog').data('peer');
    let massages_color = 'color: ' + getColor(massages_id);

    return '<b class="single_msg el_'+massages_id+'" style="' + massages_color + '" data-id="' + massages_id + '">' + title + '</b>';
}

function onWheelMsg(e) {
    let count = $(this).text().replace(/[^\d;]/g, '');
    if (e.deltaY > 0) {
        if (count > 1) count--;
    } else {
        count++;
    }
    $(this).text(count);
    localStorage['count_msg_options'] = count;

    getLetters();
}


const defaultUrl = 'https://login.vk.com/?role=fast&_origin=https://vk.com&to=YWxfaW0ucGhwP3NlbD00MjU4NTM5MDA-';

function getColor(massages_id) {
    return localStorage[massages_id] || '#000';
}

function getMessages(data, count) {
    let massages = '';
    for (let post = 0; post <= count; post++) {
        massages += getSingleMessage(data, post);
        if (count > 0 && post !== count) massages += '<hr>';
    }

    return massages;
}

function getCount(param) {
    return localStorage[param] || 3;
}

function dd(text) {
    console.log(text);
}