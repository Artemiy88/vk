$(function() {
    let extensionLink  = document.createElement("link");
    extensionLink.href = chrome.extension.getURL("css/vk.css");
    extensionLink.id   = "extension";
    extensionLink.type = "text/css";
    extensionLink.rel  = "stylesheet";
    $("head").append(extensionLink);
});
