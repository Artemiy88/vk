const defaultUrl = 'https://login.vk.com/?role=fast&_origin=https://vk.com&to=YWxfaW0ucGhwP3NlbD00MjU4NTM5MDA-';
const selectUrl = 'https://vk.com/im?sel=';
const type_count = ['по ', '№'];

$(function () {
    $('.close').unbind('click').bind('click', function (event) {
        localStorage.removeItem('url');
        event.preventDefault();
        window.close();
    });
    // getLettersCount();
    setInterval(getLettersCount, 5000);

    const counter = document.getElementById('count');
    if (counter) {
        counter.addEventListener("wheel", onWheel);
        counter.addEventListener("click", changeType);
        const start_count = getCount('count');
        const type = (localStorage['type']) ? +JSON.parse(localStorage['type']) : 0;
        $('#count').text(type_count[type] + start_count).data('type', type);
    }

    const counter_msg = document.getElementById('count_msg');
    if (counter_msg) {
        counter_msg.addEventListener("wheel", onWheelMsg);
        const start_count = getCount('count_msg');
        $('#count_msg').text(start_count);
    }

    $(document).on('click', '.single_msg', function () {
        let id = $(this).data('id');
        localStorage['url'] = selectUrl + id;
        getLettersCount();
    });

    $(document).on('click', '.back', function () {
        localStorage.removeItem('url');
        getLettersCount();
    });

});

function getLettersCount() {
    const singleUrl = localStorage['url'] || null;
    const $url = localStorage['url'] || defaultUrl;

    const xhr = new XMLHttpRequest();
    xhr.open("GET", $url, true);
    xhr.send(null);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.responseText) {
                const data = xhr.responseText;
                getFavMsgCount(data);
                let count = getCount('count') -1;
                if (singleUrl) {
                    $('.back, .count_msg').removeClass('hidden');
                    $('.count').addClass('hidden');
                    $('.body').html(getChat(data));
                } else {
                    $('.back, .count_msg').addClass('hidden');
                    $('.count').removeClass('hidden');
                    if (localStorage['type'] && JSON.parse(localStorage['type'])) {
                        $('.body').html(getSingleMessage(data, count));
                    } else {
                        $('.body').html(getMessages(data, count));
                    }
                }
            }
        }
    }
}

function getFavMsgCount(data) {
    let new_letter_count = 0;
    for (let el in localStorage) {
        const color = getColor(el);
        if (
            !isNaN(el)
            && color.slice(0, 1) === '#'
            && color !== '#000'
        ) {
            new_letter_count = $('._im_dialog_' + el + ' ._im_dialog_unread_ct', data).text() * 1;
            if (new_letter_count > 0) {
                setBage(new_letter_count + '', color);
                break;
            }
        }
    }
    if (new_letter_count === 0) {
        const other_letters_count = $('#l_msg .inl_bl.left_count', data).text();
        setBage(other_letters_count, '#2e3337');
    }
}

function setBage(text, color) {
    const $chrome = chrome.browserAction;
    $chrome.setBadgeText({text: text});
    $chrome.setBadgeBackgroundColor({color: color});
}

function getChat(data) {
    let msj_list = $('._im_peer_history.im-page-chat-contain .im-mess-stack:nth-last-child(-n+' + getCount('count_msg') + ')', data);
    let massages = '<div>';
    $.each(msj_list, function( index, value ) {
        massages += value.innerHTML;
    });
    massages += '</div>';

    return massages;
}

function getSingleMessage(data, post) {
    const text = $('.nim-dialog .nim-dialog--text-preview', data).eq(post).html();
    const title = $('.nim-dialog ._im_dialog_link', data).eq(post).text();
    const massages_id = +$('.nim-dialog ._im_dialog_link', data).eq(post).closest('.nim-dialog').data('peer');
    const li_by_id = 'li[data-list-id="' + massages_id + '"]';
    const monline = $(li_by_id + ' .nim-peer', data).hasClass('mobile') ? '<span class="mobile"></span>' : '';
    let online = $(li_by_id + ' .nim-peer', data).hasClass('online') ? '<b>●</b>' : '';
    if (monline) online = monline;
    const read = $(li_by_id, data).hasClass('nim-dialog_unread-out') ? ' &#x1f441;' : '';
    let massages_color = 'color: ' + getColor(massages_id);

    return online
            + ' <b class="single_msg" style="' + massages_color + '" data-id="' + massages_id + '">' + title + ':</b>'
            + read
            + '<div class="text_msg single_msg" data-id="' + massages_id + '">' + text + '</div>';
}

function onWheel(e) {
    const type = $(this).data('type');
    let count = $(this).text().replace(/[^\d;]/g, '');
    if (e.deltaY > 0) {
        if (count > 1) count--;
    } else {
        count++;
    }
    $(this).text(type_count[type] + count).data('type', type);
    localStorage['count'] = count;
}

function onWheelMsg(e) {
    let count = $(this).text().replace(/[^\d;]/g, '');
    if (e.deltaY > 0) {
        if (count > 1) count--;
    } else {
        count++;
    }
    $(this).text(count);
    localStorage['count_msg'] = count;
}

function changeType() {
    const count = $(this).text().replace(/[^\d;]/g, '');
    let type = +!$(this).data('type');
    $(this).data('type', type);
    localStorage['type'] = type;
    $(this).text(type_count[type] + count).data('type', type);
}

function getColor(massages_id) {
    return localStorage[massages_id] || '#000';
}

function getMessages(data, count) {
    let massages = '';
    for (let post = 0; post <= count; post++) {
        massages += getSingleMessage(data, post);
        if (count > 0 && post !== count) massages += '<hr>';
    }

    return massages;
}

function getCount(param) {
    return localStorage[param] || 3;
}

function dd(text) {
    console.log(text);
}