$(function () {
    for (let i = 1; i <= localStorage.length; i++) {
        if (!localStorage[i]) continue;
        const mass = localStorage[i] ? JSON.parse(localStorage[i]) : [];
        const name = (mass['name']) ? mass['name'] : '';
        const id = (mass['id']) ? mass['id'] : '';
        const color = (mass['color']) ? mass['color'] : '';
        const html5 = (mass['html5']) ? mass['html5'] : '';
        let text = "<tr class='old'>";
        text += "<td> <input data-count ='" + i + "' data-type               ='name' type       ='text' id    ='" + i + "_name' value  ='" + name + "' placeholder='name' /></td>";
        text += "<td> <input data-count ='" + i + "' data-type               ='id' type         ='text' id    ='" + i + "_id' value    ='" + id + "'  placeholder='vk_id'/></td>";
        text += "<td><div class         ='dies'>#</div><input data-count ='" + i + "' data-type ='color' type ='text' id           ='" + i + "_color' value='" + color + "' /></td>";
        text += "<td> <input data-count ='" + i + "' data-type               ='html5' type      ='color' id   ='" + i + "_html5' value ='" + html5 + "' class='color_F' /></td>";
        text += "<td data-count         ='" + i + "' class                   ='save' >&check;</td>";
        text += "<td data-count         ='" + i + "' class                   ='del' >&cross;</td>";
        text += "</tr>";
        $('table').append(text);
    }
    $(document).on('click', '.save', function () {
        const count = $(this).data('count');
        const name  = $(this).parent().children().children('[data-type ="name"]').val();
        const id    = $(this).parent().children().children('[data-type ="id"]').val();
        const color = $(this).parent().children().children('[data-type ="color"]').val();
        const html5 = $(this).parent().children().children('[data-type ="html5"]').val();
        const mass  = {'name': name, 'id': id, 'color': color, 'html5': html5};
        localStorage[count] = JSON.stringify(mass);
        $(this).css('color', '#0c0');
    });
    $(document).on('click', 'input', function () {
        const count = $(this).data('count');
        $('.save[data-count="' + count + '"]').css('color', '#000');
    });
    $(document).on('click', '.del', function () {
        const count = $(this).data('count');
        $(this).parent().addClass('deleted');
        setTimeout(
            function () {
                $('.deleted').remove();
            }, 300);
        localStorage.removeItem(count);
    });
    $(document).on('keyup', '[data-type="color"]', function () {
        const val = $(this).val();
        $(this).parent().parent().children().children('[data-type="html5"]').val('#' + val);
    });
    $(document).on('change', 'input[type="color"]', function () {
        const val = $(this).val().slice(1);
        $(this).parent().parent().children().children('[data-type="color"]').val(val);
    });
    $(document).on('click','#add',  function () {
        const count = $('table tr').length + 1;
        let text = "<tr class='new_tr'>";
        text += "<td> <input data-count ='" + count + "' data-type           ='name' type           ='text' id    ='name_" + count + "' value ='' placeholder='name' /></td>";
        text += "<td> <input data-count ='" + count + "' data-type           ='id' type             ='text' id    ='id_" + count + "'   value ='' placeholder='vk_id' /></td>";
        text += "<td><div class ='dies'>#</div><input data-count ='" + count + "' data-type ='color' type ='text' id ='color_" + count + "' placeholder='000000' value='' /></td>";
        text += "<td> <input data-count ='" + count + "' data-type           ='html5' type          ='color' id   ='html5_" + count + "' value ='' class='color_F' /></td>";
        text += "<td data-count         ='" + count + "' class               ='save' > &check;</td>";
        text += "<td data-count         ='" + count + "' class                   ='del' > &cross;</td>";
        text += "</tr>";
        $('table').append(text);
        setTimeout(
            function () {
                $('.new_tr').addClass('active');
            }, 10);
    });
});

function dd(text) {
    console.log(text);
}
